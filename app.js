var express = require("express");
var app = express();
var MongoClient = require('mongodb').MongoClient;
var async = require("async");
var db;
var bidTable = {};
var margin = 1.2;
var defaultCost = 0.01;
app.get('/c/:user/:id/',function(req,res){
    var id = req.params.id;
    var user = req.params.user;
    var cost;
    if(bidTable && bidTable[user] && bidTable[user][id]){
        cost = bidTable[user][id];
    }else{
        cost = defaultCost;
    }
    cost = cost*1.2;
    db.collection('statsbyid').updateOne({_id:id},{$inc: {click:1,cost:cost}},{upsert:true,w:-2},function(err){
                if (err) return console.log(err);
                //console.log('success');
    });
    db.collection('statsbyuser').updateOne({_id:user},{$inc: {click:1,cost:cost}},{upsert:true,w:-2},function(err){
                if (err) return console.log(err);
                //console.log('success');
    });
    //res.end(user+':'+id);
    res.redirect('http://m.gangzaa.net/pr/quant.php?s='+id);
});
app.get('/sb/:user/:id/:cost',function(req,res){
    var id = req.params.id;
    var user = req.params.user;
    var cost = +req.params.cost;
    if(cost <= 0){
        return res.end('err: check bid');
    }
    
    var data = {};
    data[user+'.'+id] = cost;
    db.collection('bidtable').updateOne({_id:'default'},{$set:data},{upsert:true},function(err,result){
        if(err) return res.end(err);
        
        if(bidTable && bidTable[user]){
            bidTable[user][id] = cost;
        }else if(bidTable){
            bidTable[user] = {};
            bidTable[user][id] = cost;
        }
        if(user !== 'ps21' || user !== 'jeff'){
            res.end('warning: check user');
        }else{
            res.end(JSON.stringify(bidTable));
        }
    });
});
app.get('/report',function(req,res){
    db.collection('statsbyid').find().toArray(function(err,data){
        if(err) return res.end('err');
        res.end(JSON.stringify(data));
    });
});
app.get('/quant',function(req,res){
    db.collection('statsbyuser').find().toArray(function(err,data){
        if(err) return res.end('err');
        res.end(JSON.stringify(data));
    });
});

async.auto({
    database:function(done){
        MongoClient.connect("mongodb://127.0.0.1:27017/gamed", function(err, db) ////change to HDFS
        {
            //console.log(db);
            if (err) return done(err);
            var database = db;
            done(null,database);
        });
    }
},
    function(err,result){
        if(err) throw err;
    db = result.database;
    db.collection('bidtable').findOne({_id:'default'},function(err,bidTableData){
        bidTable = bidTableData || {};
        console.log(bidTable);
        var server = app.listen(8989, function()
         {
            console.log('Listening on port %d', server.address().port);
         });
    });
    
});